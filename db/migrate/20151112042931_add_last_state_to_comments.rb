class AddLastStateToComments < ActiveRecord::Migration
  def change
    add_reference :comments, :last_state, index: true
    add_foreign_key :comments, :states, column: :last_state_id
  end
end
