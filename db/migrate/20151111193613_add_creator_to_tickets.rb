class AddCreatorToTickets < ActiveRecord::Migration
  def change
    add_reference :tickets, :creator, index: true
    add_foreign_key :tickets, :users, column: :creator_id
  end
end
