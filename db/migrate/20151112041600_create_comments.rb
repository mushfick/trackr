class CreateComments < ActiveRecord::Migration
	def up
		create_table :comments do |t|
			t.text :text
			t.references :ticket, index: true, foreign_key: true
			t.references :creator, index: true
			t.timestamps null: false
		end
	add_foreign_key :comments, :users, column: :creator_id
	end
end