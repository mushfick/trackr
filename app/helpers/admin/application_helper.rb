module Admin::ApplicationHelper
	def roles
		roles_hash = {}
		Role.available_roles.each { |role| roles_hash[role.titleize] = role }
		return roles_hash
	end
end
