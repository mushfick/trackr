module ApplicationHelper
	def title(*sections)
		unless sections.empty?
			content_for :title do
				(sections << "Trackr").join(' : ')
			end
		end
	end
	
	def admins_only(&block)
		block.call if current_user.try(:admin?)
	end
end
