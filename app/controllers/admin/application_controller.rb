class Admin::ApplicationController < ApplicationController
	before_action :authorize_admin!
	skip_after_action :verify_authorized, :verify_policy_scoped

	def index
	end

	private
	def authorize_admin!
		authenticate_user!
		unless current_user.admin?
			redirect_to root_path, alert: "You need admin access for this."
		end
	end

end
