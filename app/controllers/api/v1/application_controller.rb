class API::V1::ApplicationController < Sinatra::Base

private
def set_project
	@project = Project.find(params[:project_id])
end

def set_user
	if env["HTTP_AUTHORIZATION"].present?
		if auth_token = env["HTTP_AUTHORIZATION"].match(/(?<=token=).*/).to_s
			@user = User.find_by!(api_key: auth_token)
			return @user if @user.present?
		end
	end
	unauthenticated!
end

def unauthenticated!
	halt 401, { error: "Unauthenticated" }.to_json
end

end