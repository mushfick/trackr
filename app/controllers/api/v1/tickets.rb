require "sinatra"

module API
	module V1
		class Tickets < API::V1::ApplicationController
			before do
				headers "Content-Type" => "text/json"
				set_user
				set_project
			end

			get "/:id" do
				ticket = @project.tickets.find(params[:id])
				unless TicketPolicy.new(@user, ticket).show?
					halt 404, "The ticket could not be found."
				end
				TicketSerializer.new(ticket).to_json
			end

			private
			def params
				hash = env["action_dispatch.request.path_parameters"].merge!(super)
				HashWithIndifferentAccess.new(hash)
			end
		end
	end
end