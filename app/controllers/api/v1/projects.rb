require "sinatra"

module API
	module V1
		class Projects < API::V1::ApplicationController
			before do
				headers "Content-Type" => "text/json"
				set_user
			end

			get "/:id" do
				# params.to_json
				project = Project.find(params[:id])
				unless ProjectPolicy.new(@user, project).show?
					halt 404, "The ticket could not be found."
				end
				ProjectSerializer.new(project).to_json
			end
		end
	end
end