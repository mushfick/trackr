class User < ActiveRecord::Base
	
	has_many :roles
	# Include default devise modules. Others available are:
	# :confirmable, :lockable, :timeoutable and :omniauthable
	devise :database_authenticatable, :registerable,
	     :recoverable, :rememberable, :trackable, :validatable
	
	#filter out archived users from index etc.
	scope :excluding_archived,	-> { where(archived: nil) }

	def to_s
		"#{email} (#{admin? ? "Admin" : "User"})"
	end

	def archive
		self.update(archived: true)
	end

	def active_for_authentication?
		super && archived.nil?
	end

	def inactive_message
		archived.nil? ? super : :archived
	end

	def role_on(project)
		roles.find_by(project_id: project).try(:name)
	end

	def generate_api_key
		self.update_column(:api_key, SecureRandom.hex(16))
	end
end
