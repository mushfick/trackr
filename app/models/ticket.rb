class Ticket < ActiveRecord::Base
  belongs_to :project
  belongs_to :creator, class_name: "User"
  belongs_to :state

  has_many :comments, dependent: :destroy

  validates :name, presence: true
  validates :description, presence: true, length: { minimum: 10 }

  before_create :assign_default_state

  private 
  def assign_default_state
  	self.state ||= State.default
  end
end
