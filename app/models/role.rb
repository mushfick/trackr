class Role < ActiveRecord::Base
  belongs_to :project
  belongs_to :user

  def self.available_roles
  	['manager', 'editor', 'auditor']
  end
end
