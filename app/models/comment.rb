class Comment < ActiveRecord::Base
	belongs_to :ticket
	belongs_to :creator, class_name: "User"
	belongs_to :state
	belongs_to :last_state, class_name: "State"

	before_create :set_last_state
	after_create :set_ticket_state

	delegate :project, to: :ticket
	
	validates :text, presence: true

	scope :persisted, -> { where.not(id: nil) }

	private
	def set_last_state
		self.last_state = ticket.state
	end
	def set_ticket_state
		ticket.state = state
		ticket.save!
	end
end