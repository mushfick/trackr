class State < ActiveRecord::Base
	
	scope :default, -> { find_by(default: true) }
	
	def to_s
		name
	end
	
	def make_default!
		State.update_all(default: false)
		update!(default: true)
	end
end
