class TicketSerializer < ActiveModel::Serializer
	attributes :id, :name, :description, :project_id, :creator_id
	has_one :state		# Serializers do not care about ownership, belongs_to is simplified as has_one
end
