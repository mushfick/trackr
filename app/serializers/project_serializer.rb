class ProjectSerializer < ActiveModel::Serializer
	attributes :id, :name, :description

	self.root = false
end
