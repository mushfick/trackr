FactoryGirl.define do
	factory :user do
		sequence(:email) { |num| "sample#{num}@user.com"}
		password "password"

		trait :admin do
			admin true
		end
	end
end