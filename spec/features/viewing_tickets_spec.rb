require "rails_helper"
RSpec.feature "Users can view tickets" do
	before do
		user = FactoryGirl.create(:user)

		project = FactoryGirl.create(:project, name: "Sample Project")
		FactoryGirl.create(:ticket, project: project,
		name: "Sample Ticket",
		description: "This is just a sample ticket",
		creator: user)
		
		another_project = FactoryGirl.create(:project, name: "Just Another Project")
		FactoryGirl.create(:ticket, project: another_project,
		name: "Another ticket", description: "Oh looky looky, another ticket",
		creator: user)

		visit "/"
	end
	scenario "for a given project" do
		click_link "Sample Project"
		expect(page).to have_content "Sample Ticket"
		expect(page).to_not have_content "Not a sample ticket."
		click_link "Sample Ticket"
		
		within("#ticket h2") do
			expect(page).to have_content "Sample Ticket"
		end
		expect(page).to have_content "This is just a sample ticket"
	end
end