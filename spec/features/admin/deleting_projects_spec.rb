require "rails_helper"

RSpec.feature "Can delete existing projects" do
	scenario "deletes successfully" do
		FactoryGirl.create(:project, name: "Sample Project")

		visit "/"
		click_link "Sample Project"
		click_link "Delete Project"

		expect(page).to have_content "Project deleted."
		expect(page.current_url).to eq projects_url
		expect(page).to have_no_content "Sample Project"
	end
end