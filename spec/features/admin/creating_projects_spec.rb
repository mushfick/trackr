require "rails_helper"

RSpec.feature "Can create new projects" do
	let(:admin) { FactoryGirl.create(:user, :admin) }
	before do
		login_as(admin)
		visit "/"

		click_link "New Project"
	end

	scenario "using valid attributes" do
		fill_in "Name", with: "Sample Project"
		fill_in "Description", with: "This is a sample project."
		click_button "Create Project"

		expect(page).to have_content "Project has been created."

		project = Project.find_by_name "Sample Project"
		expect(page.current_url).to eq project_url(project)

		title = "Sample Project : Projects : Trackr"
		expect(page).to have_title title
	end

	scenario "using invalid attributes" do
		click_button "Create Project"

		expect(page).to have_content "Name can't be blank"
	end
end