require "rails_helper"
RSpec.feature "Can create new tickets" do
	let!(:user) { FactoryGirl.create(:user) }
	let(:project) { FactoryGirl.create(:project, name: "Sample Project") }

	before do
		login_as(user)
		visit project_path(project)
		click_link "New Ticket"
	end

	scenario "using valid attributes" do
		fill_in "Name", with: "Non-standards compliance"
		fill_in "Description", with: "This is a description"
		click_button "Create Ticket"

		expect(page).to have_content "Ticket has been created."
	end

	scenario "using invalid attributes" do
		click_button "Create Ticket"

		expect(page).to have_content "Ticket has not been created."
		expect(page).to have_content "Name can't be blank"
		expect(page).to have_content "Description can't be blank"
	end
	scenario "with an invalid description" do
		fill_in "Name", with: "Non-standards compliance"
		fill_in "Description", with: "It sucks"
		click_button "Create Ticket"

		expect(page).to have_content "Ticket has not been created."
		expect(page).to have_content "Description is too short"
	end
	scenario "with valid attributes" do
		fill_in "Name", with: "Non-standards compliance"
		fill_in "Description", with: "My pages are ugly!"
		click_button "Create Ticket"
		
		expect(page).to have_content "Ticket has been created."
		
		within("#ticket") do
		    expect(page).to have_content "Creator: #{user.email}"
		end
	end
end