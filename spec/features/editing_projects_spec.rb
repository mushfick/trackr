require "rails_helper"

RSpec.feature "Can modify existing projects" do
	before do
		FactoryGirl.create(:project, name: "Sample Project")

		visit "/"
		click_link "Sample Project"
		click_link "Edit Project"
	end

	scenario "using valid attributes" do
		fill_in "Name", with: "Edited Sample Project"
		click_button "Update Project"

		expect(page).to have_content "Project has been updated."
		expect(page).to have_content "Edited Sample Project"
	end

	scenario "using invalid attributes" do
		fill_in "Name", with: ""
		click_button "Update Project"

		expect(page).to have_content "Name can't be blank"
	end
end
