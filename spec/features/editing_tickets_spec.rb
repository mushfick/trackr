require "rails_helper"
RSpec.feature "Can edit existing tickets" do
	let!(:user) { FactoryGirl.create(:user) }
	let(:project) { FactoryGirl.create(:project) }
	let(:ticket) { FactoryGirl.create(:ticket, project: project, creator: user) }
	
	before do
		visit project_ticket_path(project, ticket)
		click_link "Edit Ticket"
	end

	scenario "with valid attributes" do
		fill_in "Name", with: "Editing a ticket"
		click_button "Update Ticket"
		expect(page).to have_content "Ticket has been updated."
		within("#ticket h2") do
			expect(page).to have_content "Editing a ticket"
			expect(page).not_to have_content ticket.name
		end
	end

	scenario "with invalid attributes" do
		fill_in "Name", with: ""
		click_button "Update Ticket"
		expect(page).to have_content "Ticket has not been updated."
	end
end