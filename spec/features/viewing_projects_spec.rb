require "rails_helper"

RSpec.feature "Can view projects" do
	scenario "with project details" do
		project = FactoryGirl.create(:project, name: "Sample Project")

		visit "/"
		click_link "Sample Project"
		expect(page.current_url).to eq project_url(project)
	end
end